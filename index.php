<?php

echo '
<head>
    
    <title>Convocation - NIT Calicut</title>    
    
    <!--    Meta-tags-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Amruthlal M">
    <meta name="description" content="Emergency Student Support Fund aims to support the monetary needs of full time B.Tech CSE students of NIT Calicut.">
    
    <!--    CSS files and fonts-->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="index.css" type="text/css">    
</head>


';
include('header.php');

echo '
<div id="body-wrapper">
    <div id="guests">
    <div id="chief-guest" class="guest-wrapper">
    
        <div id="photo-chief-guest-div">
        <img src="images/chief-guest.jpg" id="photo-chief-guest">
        </div>
        <div id="description-cheif-guest" class="description-guest">
            <p>Sri Ramnath Kovind
                <br>
                Hon\'ble President Of India
            </p>
                    
        </div>
    </div>
    <div id="guest-of-honor" class="guest-wrapper">
        <div id="photo-goh-div">
        <img src="images/guest-of-honor.jpg" id="photo-goh">
        </div>
        <div id="description-goh" class="description-guest">
            <p>Sri Ramnath Kovind
                <br>
                Hon\'ble President Of India
            </p>
                    
        </div>
    </div>
    </div>
    <div id="important-links">
    </div>
</div>
';

include('footer.php');
?>
